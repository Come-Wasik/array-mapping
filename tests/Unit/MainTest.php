<?php

declare(strict_types=1);

use Nolikein\ArrayMapping\Exceptions\EmptyPath;
use Nolikein\ArrayMapping\MagicArray;
use Nolikein\ArrayMapping\MagicArrayFacade;

$dummyArray = [
    'data1' => 'value1',
    'data2' => [
        'data3' => 'value2',
        'data4' => 'value3',
    ],
    12 => 42,
];

it('can retrieve', function () use ($dummyArray): void {
    // The mapping path is void
    $dataFetched = (new MagicArray())->get($dummyArray, '');
    $this->assertNull($dataFetched);

    // The mapping go to a data which does not exists
    $dataFetched = (new MagicArray())->get($dummyArray, 'inexistantData');
    $this->assertNull($dataFetched);

    // The mapping path fetch a one level data
    $dataFetched = (new MagicArray())->get($dummyArray, 'data1');
    $this->assertEquals('value1', $dataFetched);
    $this->assertIsString($dataFetched);

    // The mapping path fetch a two level data
    $dataFetched = (new MagicArray())->get($dummyArray, 'data2.data3');
    $this->assertEquals('value2', $dataFetched);
    $this->assertIsString($dataFetched);

    // The mapping path fetch a two level data and the separator changes
    $dataFetched = (new MagicArray())->get($dummyArray, 'data2-data3', '-');
    $this->assertEquals('value2', $dataFetched);
    $this->assertIsString($dataFetched);

    // The mapping path fetch an integer name in the array
    $dataFetched = (new MagicArray())->get($dummyArray, '12');
    $this->assertEquals(42, $dataFetched);
    $this->assertIsInt($dataFetched);
    $this->assertIsNotString($dataFetched);
});

it('cannot retrieve from empty path', function (): void {
    // The mapping path and the data are void
    $this->expectException(EmptyPath::class);
    (new MagicArray())->make('', '');
});

it('can get from facade', function () use ($dummyArray): void {
    // The mapping path is void
    $dataFetched = MagicArrayFacade::get($dummyArray, '');
    $this->assertNull($dataFetched);
});

it('can store', function () use ($dummyArray): void {
    // The mapping path change a one level data
    $dataFetched = (new MagicArray())->set($dummyArray, 'data1', 'value42');
    // Previous data is preserved
    $this->assertEquals('value2', $dataFetched['data2']['data3']);
    // New data is registered
    $this->assertEquals('value42', $dataFetched['data1']);
    $this->assertIsArray($dataFetched);

    // The mapping path change a two level data
    $dataFetched = (new MagicArray())->set($dummyArray, 'data2.data3', 'value42');
    $this->assertIsArray($dataFetched);
    $this->assertEquals('value42', $dataFetched['data2']['data3']);

    // The mapping path change a three level data
    $dataFetched = (new MagicArray())->set($dummyArray, 'data2.data3.data4', 'value42');
    $this->assertIsArray($dataFetched);
    $this->assertEquals('value42', $dataFetched['data2']['data3']['data4']);

    // The mapping path change an integer name in the array
    $dataFetched = (new MagicArray())->set($dummyArray, '12', 63);
    $this->assertIsArray($dataFetched);
    $this->assertEquals(63, $dataFetched[12]);
    $this->assertIsInt($dataFetched[12]);
    $this->assertIsNotString($dataFetched[12]);
});

it('can store with separator', function (string $separator) use ($dummyArray): void {
    // The mapping path change a two level data and the separator change
    $dataFetched = (new MagicArray())->set($dummyArray, 'data2' . $separator . 'data3', 'value42', $separator);
    $this->assertIsArray($dataFetched);
    $this->assertEquals('value42', $dataFetched['data2']['data3']);
})->with([
    '-',
    '_',
]);

it('cannot store from empty path', function () use ($dummyArray): void {
    // The mapping path and the data are void
    $this->expectException(EmptyPath::class);
    (new MagicArray())->set($dummyArray, '', '');
});

it('can store from facade', function (): void {
    // The mapping path and the data are void
    $dataFetched = MagicArrayFacade::set([], 'hello', 'world');
    $this->assertIsArray($dataFetched);
    $this->assertEquals('world', $dataFetched['hello']);
});

it('can make new', function () {
    // The mapping path change a one level data
    $res = (new MagicArray())->make('tree', 'dummy');
    $this->assertIsArray($res);
    $this->assertEquals('dummy', $res['tree'] ?? null);

    // The mapping path change a two level data
    $res = (new MagicArray())->make('root.leaf', 'dummy');
    $this->assertIsArray($res);
    $this->assertEquals('dummy', $res['root']['leaf'] ?? null);

    // The mapping path change a two level data and the separator change
    $res = (new MagicArray())->make('root-leaf', 'dummy', '-');
    $this->assertIsArray($res);
    $this->assertEquals('dummy', $res['root']['leaf'] ?? null);

    // The mapping path change a three level data
    $res = (new MagicArray())->make('root.mid.leaf', 'dummy');
    $this->assertIsArray($res);
    $this->assertEquals('dummy', $res['root']['mid']['leaf'] ?? null);
});

it('cannot make new from empty path', function (): void {
    // The mapping path and the data are void
    $this->expectException(EmptyPath::class);
    (new MagicArray())->make('', '');
});

it('can make from facade', function (): void {
    // The mapping path change a one level data
    $res = (new MagicArray())->make('tree', 'dummy');
    $this->assertIsArray($res);
    $this->assertEquals('dummy', $res['tree'] ?? null);
});
