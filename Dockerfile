FROM php:8.3.13

# Add xdebug
ARG XDEBUG_VERSION="xdebug-3.3.2"

RUN yes | pecl install ${XDEBUG_VERSION} \
    && docker-php-ext-enable xdebug \
    && echo "xdebug.mode=develop,debug,coverage" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request = no" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

WORKDIR /

RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/local/bin/ --filename=composer

RUN apt-get update \
    && apt-get install -y git zip libzip-dev  \
    && apt-get -y autoremove \
    && apt-get clean

RUN docker-php-ext-configure zip

RUN docker-php-source extract \
    && docker-php-ext-install zip \
    && docker-php-source delete
