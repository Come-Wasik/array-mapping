# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 2.0.0 - 2020-10-31

Change the namespace of ArrayMapping from `Nolikein` to `Nolikein\ArrayMapping`.
Add `gitlab-ci.yaml` for tests from gitlab.
Change the `ArrayMapping::update` method to `ArrayMapping::assign`.
Add an interface named `ArrayMappingInterface` that ArrayMapping class implements.
Changing the test directory tree

## 1.0.2

Fixing error when try to add an array which not exist.

## 1.0.1

Modify the readme since the project is available from packagist.

## 1.0.0

Initial stable release.