<?php

namespace Nolikein\ArrayMapping;

class MagicArrayFacade
{
    /**
     * Access to a nested array by using a path.
     *
     * @param array<int|string, mixed> $array The array to browse
     * @param non-empty-string $path The path used to access the data
     * @param non-empty-string $separator The separator used in the path
     *
     * @return mixed|null Return null if none data exists at path, mixed value if exists
     */
    public static function get(array $array, string $path, string $separator = '.'): mixed
    {
        $i = new MagicArray();

        return $i->get($array, $path, $separator);
    }

    /**
     * Make a new array using a path.
     *
     * @param non-empty-string $path The path used to access the data
     * @param mixed $data The data to assign
     * @param non-empty-string $separator The separator used in the path
     *
     * @return array<int|string, mixed> Return an array with the data nested in the array given as parameter
     */
    public static function make(string $path, $data, string $separator = '.'): array
    {
        $i = new MagicArray();

        return $i->make($path, $data, $separator);
    }

    /**
     * Assign a value in the nested array by using a path.
     *
     * @param array<int|string, mixed> $array The array to browse
     * @param non-empty-string $path The path used to access the data
     * @param mixed $data The data to assign
     * @param non-empty-string $separator The separator used in the path
     *
     * @return array<int|string, mixed> Return an array with the data nested in the array given as parameter
     */
    public static function set(array $array, string $path, $data, string $separator = '.'): array
    {
        $i = new MagicArray();

        return $i->set($array, $path, $data, $separator);
    }
}
