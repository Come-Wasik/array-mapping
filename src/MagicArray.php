<?php

namespace Nolikein\ArrayMapping;

use Nolikein\ArrayMapping\Exceptions\EmptyPath;

class MagicArray
{
    /**
     * Retrieve a value from an array by using a path.
     *
     * @param array<int|string, mixed> $array The array to browse
     * @param non-empty-string $path The path used to access the data
     * @param non-empty-string $separator The separator used in the path
     *
     * @return mixed|null Return null if none data exists at path, mixed value if exists
     */
    public function get(array $array, string $path, string $separator = '.'): mixed
    {
        foreach (explode($separator, $path) as $key) {
            if ( ! array_key_exists($key, $array)) {
                return null;
            }

            $array = $array[$key];
        }

        return $array;
    }

    /**
     * Make a new array using a path.
     *
     * @param non-empty-string $path The path used to access the data
     * @param mixed $data The data to assign
     * @param non-empty-string $separator The separator used in the path
     *
     * @throws EmptyPath If given path is empty
     *
     * @return array<int|string, mixed> Return an array with the data nested in the array given as parameter
     */
    public function make(string $path, $data, string $separator = '.'): array
    {
        if ('' === $path) {
            throw new EmptyPath(sprintf('The given path is empty.'));
        }

        $array = [];

        // We explode directly the path
        // Because the most common usage is two path
        $xPath = explode($separator, $path);

        // It is faster for the common usages to perform direct return
        $count = count($xPath);
        if (1 === $count) {
            $array[$xPath[0]] = $data;

            return $array;
        }
        if (2 === $count) {
            $array[$xPath[0]][$xPath[1]] = $data;

            return $array;
        }
        if (3 === $count) {
            $array[$xPath[0]][$xPath[1]][$xPath[2]] = $data;

            return $array;
        }

        // For more than 3 path, we will automatize it
        $firstKey = array_pop($xPath);

        return array_reduce(
            array_reverse($xPath),
            function ($carry, $key): array {
                $new = [];
                $new[$key] = $carry;

                return $new;
            },
            [
                $firstKey => $data,
            ]
        );
    }

    /**
     * Make using a path.
     *
     * @param array<int|string, mixed> $array The array to browse
     * @param non-empty-string $path The path used to access the data
     * @param mixed $data The data to assign
     * @param non-empty-string $separator The separator used in the path
     *
     * @return array<int|string, mixed> Return an array with the data nested in the array given as parameter
     */
    public function set(array $array, string $path, $data, string $separator = '.'): array
    {
        if ('' === $path) {
            throw new EmptyPath(sprintf('The given path is empty.'));
        }

        // If enter the if statement, the path do not have a separator
        if (false === ($separatorPos = strpos($path, $separator))) {
            $array[$path] = $data;

            return $array;
        }

        // We will call our method recursively with the next path
        /** @var non-empty-string $key */
        $key = substr($path, 0, $separatorPos);

        /** @var non-empty-string $path */
        $path = substr(
            // @phpstan-ignore argument.type
            strstr($path, $separator),
            1,
            strlen($path) - 1
        );

        // We should olny treat array
        if ( ! \key_exists($key, $array) || ! is_array($array[$key])) {
            $array[$key] = [];
        }
        $array[$key] = $this->set($array[$key], $path, $data, $separator);

        return $array;
    }
}
